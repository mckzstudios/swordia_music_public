# Swordia Music Files

These music files are part of Swordia.

## Authors

Check the [music-license](music-license.md) file for a list.

In addition, "Ride of the Valkyries" is a public domain recording of a public domain song, taken from here: [Ride of the Valkyries.ogg](http://upload.wikimedia.org/wikipedia/commons/archive/2/29/20100528231037%21Richard_Wagner_-_Ride_of_the_Valkyries.ogg).
